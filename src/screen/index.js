import HomeScreen from './HomeScreen';
import LoginScreen from './LoginScreen';
import Camera from './CameraScreen';
import WebScreen from './WebScreen';


export {HomeScreen, LoginScreen, Camera, WebScreen}